//
//  DetailTableViewCell.swift
//  NewsFeed
//
//  Created by IdolEnvy on 4/9/18.
//  Copyright © 2018 IdolTIMEs. All rights reserved.
//

import Foundation
import SDWebImage

class DetailTableViewCell: UITableViewCell {
  @IBOutlet var newsImage: UIImageView!
  @IBOutlet var newsTitle: UILabel!
  @IBOutlet var newsDescription: UILabel!
  @IBOutlet var newsUpdated: UILabel!
  
  func setNews(news: News) {
    newsImage?.sd_setImage(with: URL(string: news.imageURL),
                           placeholderImage: UIImage(named: "default"),
                           options: .refreshCached,
                           completed: nil)
    
    newsTitle.text = news.name
    newsDescription.text = news.detail
    newsUpdated.text = news.updated
  }
}
