//
//  MainTableViewCell.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import UIKit
import SDWebImage

class MainTableViewCell: UITableViewCell {
  @IBOutlet var newsImage: UIImageView!
  @IBOutlet var newsTitle: UILabel!
  @IBOutlet var newsDescription: UILabel!
  @IBOutlet var newsUpdated: UILabel!
  
  func setNews(news: News) {
    newsImage?.sd_setImage(with: URL(string: news.imageURL),
                          placeholderImage: UIImage(named: "default"),
                          options: .refreshCached,
                          completed: nil)
    
    newsTitle.text = news.name
    newsDescription.text = news.detail
    newsUpdated.text = news.updated
  }
}
