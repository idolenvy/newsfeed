//
//  String+Contain.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation

extension String {
  func contains(find: String) -> Bool {
    return self.range(of: find) != nil
  }
  
  var utf8Encoded: Data {
    return data(using: .utf8)!
  }
}
