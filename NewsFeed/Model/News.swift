//
//  NewsModel.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation

public class NewsResponse: Decodable {
  var news: [News]
}

public class News: Decodable {
  var name: String
  var imageURL: String
  var detail: String
  var updated: String
    
    init() {
        name = ""
        imageURL = ""
        detail = ""
        updated = ""
    }
}

