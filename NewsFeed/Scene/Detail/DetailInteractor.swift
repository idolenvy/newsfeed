//
//  DetailInteractor.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright (c) 2561 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol DetailBusinessLogic {
  func fetchDetail(request: Detail.DetailSet.Request)
}

protocol DetailDataStore {
  var news: News { get set }
}

class DetailInteractor: DetailBusinessLogic, DetailDataStore {
  var presenter: DetailPresentationLogic?
  var news: News = News()
  
  func fetchDetail(request: Detail.DetailSet.Request) {
    presenter?.presentDetail(response: Detail.DetailSet.Response(news: news))
  }
}
