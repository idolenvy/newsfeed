//
//  DetailModels.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright (c) 2561 Allianz Technology. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Detail {
  struct DetailSet {
    struct Request { }
    
    struct Response {
      var news: News
    }
    
    struct ViewModel {
      var news: News
    }
  }
}
