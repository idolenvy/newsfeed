//
//  DetailViewController+TableView.swift
//  NewsFeed
//
//  Created by IdolEnvy on 4/9/18.
//  Copyright © 2018 IdolTIMEs. All rights reserved.
//

import UIKit

extension DetailViewController: UITableViewDataSource {
  
  func setUpTableView() {
    tableView?.estimatedRowHeight = 500
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    var cellIdentifier = "DetailTableViewCell"
    if UIDevice.current.orientation.isLandscape {
      cellIdentifier = "DetailLandscapeTableViewCell"
    }
    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                   for: indexPath) as? DetailTableViewCell else {
                                                    return UITableViewCell()
    }
    
    cell.setNews(news: news)
    cell.selectionStyle = .none
    return cell
  }
}

