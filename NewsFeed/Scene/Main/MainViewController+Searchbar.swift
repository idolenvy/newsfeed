//
//  MainViewController+Searchbar.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import UIKit

extension MainViewController: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    tableView?.reloadData()
  }
}
