//
//  MainViewController+TableView.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import UIKit

extension MainViewController: UITableViewDataSource {
  
  func setUpTableView() {
    tableView?.estimatedRowHeight = 235.0
    tableView?.rowHeight = 235.0
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isSearch {
      return searchNews.count
    } else {
      return news.count
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if UIDevice.current.orientation.isLandscape {
      return 135.0
    } else {
      return 235.0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    var cellIdentifier = "MainTableViewCell"
    if UIDevice.current.orientation.isLandscape {
      cellIdentifier = "MainLandscapeTableViewCell"
    }
    guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                   for: indexPath) as? MainTableViewCell else {
                                                    return UITableViewCell()
    }
    
    if isSearch {
      cell.setNews(news: searchNews[indexPath.row])
    } else {
      cell.setNews(news: news[indexPath.row])
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let noLoadmore = router?.dataStore?.noLoadMoreData,
      !isSearch else { return }
    
    if indexPath.row == news.count - 1 && !noLoadmore {
      fetchNews()
    }
  }
}

extension MainViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    if isSearch {
      selectNews(news: searchNews[indexPath.row])
    } else {
      selectNews(news: news[indexPath.row])
    }
    
  }
}
