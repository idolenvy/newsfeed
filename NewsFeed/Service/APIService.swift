//
//  APIService.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation
import Moya
import Alamofire

public enum APIService {
  case getNewsByOffset(offset: Int)
}

extension APIService: TargetType {
  public var baseURL: URL {
    return URL(string: "https://api.myjson.com/")!
  }
  
  public var method: Moya.Method {
    return .get
  }
  
  public var path: String {
    switch self {
    case .getNewsByOffset(let offset):
      if offset == 0 {
        return "bins/1as7pf"
      } else if offset == 5 {
        return "bins/1hbzcz"
      } else if offset == 10 {
        return "bins/10pzkz"
      } else {
        return "bins/r71wj"
      }
    }
  }
  
  public var sampleData: Data {
    switch self {
    case .getNewsByOffset(let offset):
      if offset == 0 {
        return "{ \"news\": [ {\"name\": \"news1\",\"imageName\": \"default.png\",\"detail\": \"news1 details\",\"updated\": \"2018-10-01 00:01\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\"} ] }".utf8Encoded
      } else if offset == 5 {
        return "{ \"news\": [ {\"name\": \"news2\",\"imageName\": \"default.png\",\"detail\": \"news2 details\",\"updated\": \"2018-10-01 00:02\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\",\"name\": \"news3\",\"imageName\": \"default.png\",\"detail\": \"news3 details\",\"updated\": \"2018-10-01 00:03\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\" ]} }".utf8Encoded
      } else if offset == 10 {
        return "{ \"news\": [ {\"name\": \"news3\",\"imageName\": \"default.png\",\"detail\": \"news3 details\",\"updated\": \"2018-10-01 00:03\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\"}, \"name\": \"news3\",\"imageName\": \"default.png\",\"detail\": \"news3 details\",\"updated\": \"2018-10-01 00:03\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\",\"name\": \"news3\",\"imageName\": \"default.png\",\"detail\": \"news3 details\",\"updated\": \"2018-10-01 00:03\",\"imageURL\": \"https://scontent.fbkk15-1.fna.fbcdn.net/v/t31.0-8/18319040_1368155613270587_8977103248937197468_o.jpg?oh=eab14bc80c1946c1d8d7f66efcee5caa&oe=5B496475\"] }".utf8Encoded
      } else {
        return "{ \"news\": [] }".utf8Encoded
      }
    }
  }
  
  public var parameters: [String: Any]? {
    return [:]
  }
  
  public var task: Task {
    let encoding: ParameterEncoding = method == .get ? Alamofire.URLEncoding.default : Alamofire.JSONEncoding.default
    return Task.requestParameters(parameters: parameters!, encoding: encoding)
  }
  
  public var headers: [String : String]? {
    return nil
  }
}
