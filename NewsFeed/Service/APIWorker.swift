//
//  APIWorker.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation
import Moya
import Result

public enum APIError: Swift.Error {
  case noNews
  case noConnection
  case webService
}

public class APIWorker {
  public static private(set) var shared = APIWorker()
  
  var provider: MoyaProvider<APIService>
  
  public init(provider: MoyaProvider<APIService> = MoyaProvider<APIService>()) {
    self.provider = provider
  }
  
  public func getNewsByOffset(offset: Int,
                              completion: @escaping (Result<[News], APIError>) -> Void) {
    provider.request(.getNewsByOffset(offset: offset)) { result in
      switch result {
        case .success(let moyaResponse):
          let statusCode = moyaResponse.statusCode
          guard statusCode == 200,
            let response = try? JSONDecoder().decode(NewsResponse.self, from: moyaResponse.data) else {
              completion(.failure(.webService))
              return
          }
          
          guard response.news.count != 0 else {
            completion(.failure(.noNews))
            return
          }
          
          completion(.success(response.news))
        case .failure:
          completion(.failure(.webService))
      }
    }
  }
}
