//
//  NewsFeedTests.swift
//  NewsFeedTests
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import XCTest

@testable import Moya
@testable import NewsFeed

class NewsFeedAPIWorkerTests: XCTestCase {
    func test_get_api_with_offset_0_and_get_1_sample_data() {
      let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
      APIWorker.shared.provider = provider
      
      APIWorker.shared.getNewsByOffset(offset: 0) { result in
        switch result {
        case .success(let news):
          XCTAssertEqual(news.count, 1)
        default:
          break
        }
      }
    }
  
  func test_get_api_with_offset_5_and_get_2_sample_data() {
    let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
    APIWorker.shared.provider = provider
    
    APIWorker.shared.getNewsByOffset(offset: 5) { result in
      switch result {
      case .success(let news):
        XCTAssertEqual(news.count, 2)
      default:
        break
      }
    }
  }
  
  func test_get_api_with_offset_10_and_get_3_sample_data() {
    let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
    APIWorker.shared.provider = provider
    
    APIWorker.shared.getNewsByOffset(offset: 10) { result in
      switch result {
      case .success(let news):
        XCTAssertEqual(news.count, 3)
      default:
        break
      }
    }
  }
  
  func test_get_api_with_offset_15_and_get_0_sample_data() {
    let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
    APIWorker.shared.provider = provider
    
    APIWorker.shared.getNewsByOffset(offset: 15) { result in
      switch result {
      case .failure(let error):
        XCTAssertEqual(error, .noNews)
      default:
        break
      }
    }
  }
  
  func test_get_api_with_any_offset_and_get_failure() {
    APIWorker.shared.provider = errorStubbingProvider()
    APIWorker.shared.getNewsByOffset(offset: 15) { result in
      switch result {
      case .failure(let error):
        XCTAssertEqual(error, .webService)
      default:
        break
      }
    }
  }
  
  func errorEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(500, Data())
    }

    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func errorStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: errorEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
  
}
